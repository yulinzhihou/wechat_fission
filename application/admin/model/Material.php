<?php

namespace app\admin\model;

use think\Model;

class Material extends Model
{
    // 表名
    protected $name = 'material';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'share_time_text',
        'is_show_text'
    ];


    public function getIsShowText()
    {
        return [0=>'否',1=>'是'];
    }

    public function getIsShowTextAttr($value,$data)
    {
        $value = $value ? $value : $data['is_show'];
        $list = $this->getIsShowText();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getShareTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['share_time']) ? $data['share_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setShareTimeAttr($value)
    {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }


}
