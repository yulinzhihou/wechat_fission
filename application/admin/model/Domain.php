<?php

namespace app\admin\model;

use think\Model;

class Domain extends Model
{
    // 表名
    protected $name = 'domain';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'use_status_text',
        'shielding_text',
        'domain_type_text'

    ];


    public function getUseStatusText()
    {
        return [0=>'未使用',1=>'已使用'];
    }


    public function getUseStatusTextAttr($value,$data)
    {
        $value = $value ? $value : $data['use_status'];
        $list = $this->getUseStatusText();
        return isset($list[$value]) ? $list[$value]: '';
    }


    public function getShieldingText()
    {
        return [0=>'未屏蔽',1=>'已屏蔽'];
    }


    public function getShieldingTextAttr($value,$data)
    {
        $value = $value ? $value : $data['shielding'];
        $list = $this->getShieldingText();
        return isset($list[$value]) ? $list[$value]: '';
    }

    public function getDomainTypeText()
    {
        return [0=>'入口域名',1=>'落地域名'];
    }


    public function getDomainTypeTextAttr($value,$data)
    {
        $value = $value ? $value : $data['domain_type'];
        $list = $this->getDomainTypeText();
        return isset($list[$value]) ? $list[$value]: '';
    }




}
