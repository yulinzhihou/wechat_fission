<?php

namespace app\admin\model;

use think\Model;

class Boardcast extends Model
{
    // 表名
    protected $name = 'boardcast';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'is_show_text'
    ];


    public function getIsShowText()
    {
        return [0=>'否',1=>'是'];
    }

    public function getIsShowTextAttr($value,$data)
    {
        $value = $value ? $value : $data['is_show'];
        $list = $this->getIsShowText();
        return isset($list[$value]) ? $list[$value] : '';
    }







}
