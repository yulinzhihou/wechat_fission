<?php

return [
    'Id'                =>  '主键ID',
    'Statistics'        =>  '统计代码',
    'Destination_url'   =>  '落地URL',
    'Show_times'        =>  '展示频率',
    'Is_show'           =>  '是否展示',
    'Is_show_0'         =>  '否',
    'Is_show_1'         =>  '是'
];
