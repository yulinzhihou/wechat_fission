<?php

return [
    'Id'            =>  '主键ID',
    'Video_title'   =>  '标题',
    'Thumb_pic'     =>  '缩略图',
    'Video_src'     =>  '播放地址',
    'Share_time'    =>  '分享时间',
    'Is_show'       =>  '是否展示',
    'Is_show_0'     =>  '否',
    'Is_show_1'     =>  '是'
];
