<?php

return [
    'Id'            =>  '主键ID',
    'Appid'         =>  'APPID',
    'Appkey'        =>  'APPKEY',
    'Bind_domain'   =>  '绑定域名',
    'Is_use'        =>  '是否可用',
    'Is_use_0'      =>  '否',
    'Is_use_1'      =>  '是'
];
