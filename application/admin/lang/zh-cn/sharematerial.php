<?php

return [
    'Id'  =>  'ID',
    'Title'  =>  '分享标题',
    'Desc'  =>  '分享描述',
    'Imgurl'  =>  '分享图标',
    'Nums'  =>  '分享次数',
    'Link_id'  =>  '链接类型ID',
    'Create_time'  =>  '创建时间'
];
