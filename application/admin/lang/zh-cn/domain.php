<?php

return [
    'Id'                =>  '主键ID',
    'Domain'            =>  '域名',
    'domain_type'       =>  '域名类型',
    'domain_type_0'     =>  '入口域名',
    'domain_type_1'     =>  '落地域名',
    'Shielding'         =>  '屏蔽状态',
    'Shielding_0'       =>  '未屏蔽',
    'Shielding_1'       =>  '已屏蔽',
    'Use_status'        =>  '使用状态',
    'Use_status_0'      =>  '未使用',
    'Use_status_1'      =>  '已使用'
];
