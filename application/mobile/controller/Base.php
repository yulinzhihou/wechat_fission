<?php
/**
 * Created by PhpStorm.
 * User: yulin
 * Date: 2019/1/5
 * Time: 22:25
 */
namespace  app\mobile\controller;

use app\admin\model\Domain;
use think\Controller;
use think\Env;
use Wechat\WechatApi;

class Base extends Controller
{

    public function getDomain()
    {
        $enter = Domain::where(['status'=>0,'domain_type'=>1,'use_status'=>1])->column('url');
        $inter = Domain::where(['status'=>0,'domain_type'=>0,'use_status'=>1])->column('url');

        $url = $this->request->url(true);
        $arr = explode('/',$url);
        $topInterDomain = explode('.',$arr[2]);

        $enter_rand = count($enter) <= 1 ? $enter[0] : $enter[mt_rand(0,count($enter)-1)];
        $inter_rand = count($inter) <= 1 ? $inter[0] : $inter[mt_rand(0,count($inter)-1)];

        $str = '0123456789qwertyuiopasdfghjklzxcvbnm';

        $newStr = str_shuffle($str);


        if (in_array($topInterDomain[2],$inter))
        {
            $url = 'http://'.$enter_rand.'/mobile.php/login/index?platid=1';
        }

        $this->init($url);
        
    }

    /**
     * 获取微信jssdk接口
     * @return false|string
     */
    public function getWechatJSSDK()
    {
        $script = load_wechat('Script');
        // 获取JsApi使用签名，通常这里只需要传 $ur l参数
        /**
         * 参数$url为当前页面URL地址（URL中含有#号的取#前面的字符串）
         * 参数timestamp为当前时间戳 (为空则自动生成)
         * 参数$noncestr为随机串 (为空则自动生成)
         * 参数$appid限定appid使用 (可选)
         */
        $url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $options = $script->getJsSign($url,time(),'',Env::get('wechat.appid'),'');
        // 处理执行结果
        if($options===FALSE){
            // 接口失败的处理
            echo $script->errMsg;
        }else{
            // 接口成功的处理.
//            return json_encode($options);
            return $options;
        }
    }

    /**
     * 获取code值，跳转获取用户openid或者用户信息
     * @param string $scope
     */
    public function getOauthRedirect($scope = 'snsapi_base')
    {
        $url = "http://" . $_SERVER['HTTP_HOST'] . '/mobile/index/index';
        // 实例微信粉丝接口
        $Oauth = load_wechat('Oauth');
        // 执行接口操作
        $result = $Oauth->getOauthRedirect($url, 'state',$scope);
        // 处理创建结果
        if($result===FALSE){
            // 接口失败的处理
            echo $Oauth->errMsg;
        }else{
            // 接口成功的处理
            header("Location:$result");
        }
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**********************************************************************/
//
//    public function getWechat1()
//    {
//        $jssdkconfig = $this->getjssdk();      //
//        $jssdkconfig = json_encode($jssdkconfig);
//        dump($jssdkconfig);die;
//
//    }
//
//
//    /**
//     * 获取jssdk
//     * @return array
//     */
//    public function getjssdk()
//    {
//        $url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
//        $options = [
//            'token'           => Env::get('wechat.token',''), // 填写你设定的key
//            'appid'           => Env::get('wechat.appid',''), // 填写高级调用功能的app id, 请在微信开发模式后台查询
//            'appsecret'       => Env::get('wechat.appsecret'),'', // 填写高级调用功能的密钥
//            'encodingaeskey'  => Env::get('wechat.encodingaeskey',''), // 填写加密用的EncodingAESKey（可选，接口传输选择加密时必需）
//            'mch_id'          => Env::get('wechat.mch_id',''), // 微信支付，商户ID（可选）
//            'partnerkey'      => Env::get('wechat.partnerkey',''), // 微信支付，密钥（可选）
//            'ssl_cer'         => Env::get('wechat.ssl_cer',''), // 微信支付，双向证书（可选，操作退款或打款时必需）
//            'ssl_key'         => Env::get('wechat.ssl_key',''), // 微信支付，双向证书（可选，操作退款或打款时必需）
//            'cachepath'       => Env::get('wechat.cachepath',''), // 设置SDK缓存目录（可选，默认位置在Wechat/Cache下，请保证写权限）
//        ];
//        //分享
//        $weObj = new WechatApi($options);
//        $jssdk = $weObj->getJsSign($url);
//        $wxJsSdk = [
//            'debug' => false,
//            'appId' => $jssdk['appId'],
//            'timestamp' => $jssdk['timestamp'],
//            'nonceStr' => $jssdk['nonceStr'],
//            'signature' => $jssdk['signature'],
//            'jsApiList' => [
//                'openAddress', 'onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo', 'onMenuShareQZone', 'startRecord', 'stopRecord', 'onVoiceRecordEnd', 'playVoice',
//                'pauseVoice' . 'stopVoice', 'onVoicePlayEnd', 'uploadVoice', 'downloadVoice', 'chooseImage', 'previewImage', 'uploadImage', 'downloadImage', 'translateVoice', 'getNetworkType', 'openLocation',
//                'getLocation', 'hideOptionMenu', 'showOptionMenu', 'hideMenuItems', 'showMenuItems', 'hideAllNonBaseMenuItem', 'showAllNonBaseMenuItem', 'closeWindow', 'scanQRCode', 'chooseWXPay', 'openProductSpecificView'
//            ]
//        ];
//        return $wxJsSdk;
//    }

}