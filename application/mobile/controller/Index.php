<?php

namespace app\mobile\controller;

use app\admin\model\share\Material as ShareMaterialModel;

/**
 * 后台首页
 * @internal
 */
class Index extends Base
{

    protected $noNeedLogin = ['login'];
    protected $noNeedRight = ['index', 'logout'];
    protected $layout = '';

    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 后台首页
     */
    public function index()
    {
        // 实例微信粉丝接口
        $oauth = load_wechat('Oauth');
        // 执行接口操作
        $userAccessToken = $oauth->getOauthAccessToken();

        // 处理返回结果
        if($userAccessToken===FALSE){
            // 接口失败的处理
            return false;
        }else{
            // 接口成功的处理
            // 执行接口操作

            //$result = $oauth->getOauthUserinfo($userAccessToken['access_token'], $userAccessToken['openid']);
            //dump($result);die;
            //TODO:: 1.此处可以做openid获取及用户注册行为，也可以存储access_token等一系列业务逻辑
        }

    }

    /**
     * LB分享页面
     */
    public function share()
    {
        $shareData = ShareMaterialModel::where('id',1)->find();
        $jssdk = $this->getWechatJSSDK();
        $link =  'https://www.baidu.com';
        $this->assign('jssdk',$jssdk);
        $this->assign('shareData',$shareData);
        $this->assign('link',$link);
        return $this->view->fetch();
    }


    /**
     * 点击数
     */
    public function clicknums()
    {
        //TODO::分享点击次数，
    }


    /**
     * 分享落地链接
     */
    public function shareEnter()
    {
        //TODO::分享的落地链接，通过点击分享链接进入此方法，进行广告的跳转，比如第一次进主广告，第二次调用方法进另外的广告页面，第三次进。。。。。。。
    }

}
