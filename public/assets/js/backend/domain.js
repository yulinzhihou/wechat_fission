define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'domain/index',
                    add_url: 'domain/add',
                    edit_url: 'domain/edit',
                    del_url: 'domain/del',
                    multi_url: 'domain/multi',
                    table: 'domain',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                search:false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),operate:false},
                        {field: 'url', title: __('Domain'),operate:'LIKE %...%',placeholder:__('You can use fuzzy search')},
                        {field: 'domain_type', title: __('domain_type'),visible:false,searchList: {"0": __('domain_type_0'), "1": __('domain_type_1')}},
                        {field: 'domain_type_text', title: __('domain_type'),operate:false},
                        {field: 'shielding', title: __('Shielding'),visible:false,searchList: {"0": __('shielding_0'), "1": __('shielding_1')}},
                        {field: 'shielding_text', title: __('Shielding'),operate:false,formatter:Table.api.formatter.status},
                        {field: 'use_status', title: __('Use_status'),visible:false,searchList: {"0": __('use_status_0'), "1": __('use_status_1')}},
                        {field: 'use_status_text', title: __('Use_status'),operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});