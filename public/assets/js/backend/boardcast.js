define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'boardcast/index',
                    add_url: 'boardcast/add',
                    edit_url: 'boardcast/edit',
                    del_url: 'boardcast/del',
                    multi_url: 'boardcast/multi',
                    table: 'boardcast',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'statistics', title: __('Statistics')},
                        {field: 'destination_url', title: __('Destination_url'), formatter: Table.api.formatter.url},
                        {field: 'show_times', title: __('Show_times')},
                        {field: 'is_show', title: __('Is_show'),operate:false,visible:false},
                        {field: 'is_show_text', title: __('Is_show'),operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});